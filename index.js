var cors = require('cors');
require('dotenv').config();
const SECRET_KEY = process.env.SECRET_KEY;
            
const jwt = require('jsonwebtoken');
const express = require('express')
var morgan = require('morgan')
var logger = morgan('combined');

const db = require('./src/storage/mongo');

//console.log(db);

db.init().then(() => {
    var apiRouter = require('./src/router/index');
    const app = express();
    
    app.use(express.json());
    app.use(cors());
    app.use(function (req, res, next) {
        logger(req, res, function (err) {
            if (err) return done(err)
        })
        next();
    });
    
    app.use('/api', apiRouter);
   
    const PORT = process.env.NODE_DOCKER_PORT;
    app.listen(PORT, () => {
        console.log(`Listening at http://localhost:${PORT}`)
    })
}).catch((err) => {
    console.error(err);
});






