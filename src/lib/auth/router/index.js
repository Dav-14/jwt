const express = require("express");
const router  = express.Router();
const isAuthenticated = require('../middlewares/isAuthenticated');
const authController = require('../auth.controller');

router.post('/session', authController.login);
router.delete('/session', isAuthenticated, authController.deleteSession);
router.get('/me', isAuthenticated, authController.getSession);
router.post('/createUser', authController.create);

module.exports = router;