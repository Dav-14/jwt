const mongoose = require('mongoose');
const { Schema, model } = mongoose; 
const bcrypt = require('bcrypt');

const userSchema = new Schema({
  username:  String, // String is shorthand for {type: String}
  email: String,
  password: String,
});

module.exports.User = model('user', userSchema);

//Will be delete
module.exports.createUser = async ({username, email, password}) => {
    let newUser = new this.User({
        username:username, 
        email: email, 
        password: password
    });
    await newUser.save();
    //console.log(newUser);
    return newUser;
}

module.exports.getUser = async ({ email }) => {
  const user = this.User.findOne({
    email: email
  });

  return user;
};

module.exports.generateNewHash = async ({email, password}) => {
  let user = await this.getUser({email});
  //If the user exists
  if (user && password){
      let salt = await bcrypt.genSalt(12);
      let hash = await bcrypt.hash(password, salt);
      user.password = hash;
      console.log(hash)
      await user.save();
      return user;
        
  }else{
    throw new Error('GENERATE_ERROR');
  }
}

