const jwt = require('../../jwt/jwt.controller');
const authModel = require("../auth.model");

module.exports = async (req,res,next) => {
    var authheader = req.headers.authorization;
    if (!authheader) {
        var err = new Error('You are not authenticated!');
        res.setHeader('WWW-Authenticate', 'Basic');
        err.status = 401;
        return next(err)
    }

    if (String(authheader).startsWith('Bearer ')){
        let token = authheader.split(' ')[1];
        try {
            token = await jwt.verifyToken(token); 
        } catch (error) {
            throw error;
        }
        
        console.log(authheader);
        console.log(token);
    
        var email = token["email"];
        var expireTo = token["expireTo"];
        var expireFrom = token["expireFrom"];
        
        //Check if we got the emails
        if (email && expireTo != null && authModel.getUser({email}) != null) {       
            next();    
        }
        else {
            var err = new Error('You are not authenticated!');
            res.setHeader('WWW-Authenticate', 'Basic');
            err.status = 401;
            return next(err);
        }
    }else{
        //IF COOKIE CHECK FOR A NEXT TIME
        var err = new Error('You are not authenticated!');
        res.setHeader('WWW-Authenticate', 'Basic');
        err.status = 401;
        return next(err);
    }


    
 
}