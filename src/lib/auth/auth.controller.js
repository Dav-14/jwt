const { User, generateNewHash } = require('./auth.model');
const { createUser, getUser } = require('./auth.model');
const _ = require('lodash');
const { isEmpty } = require('lodash');
const jwt = require('../jwt/jwt.controller');
const bcrypt = require('bcrypt');
const authRessource = require('./auth.ressource');
const { isEmail } = require('validator');

Date.prototype.addDays = function(days) {
    var date = new Date(this.valueOf());
    date.setDate(date.getDate() + days);
    return date;
}

//Get Session from Auth Token
async function getSession(req,res){
    let email = req.body.email;
    if (email != null){
        var authheader = req.headers.authorization;
        
        if (!authheader) {
            var err = new Error('You are not authenticated!');
            res.setHeader('WWW-Authenticate', 'Basic');
            err.status = 401;
            res.status(401).send({
                message: err,
            })
        }else{
            let decodedToken = await jwt.decodeToken(authheader);
            console.log(decodedToken);
            const auth = await User.find({email: email});
            res.send(auth);
        }   
    }else{
        res.status(400).send({
            message: 'USER_NOT_FOUND'
        });
    }
    
}
async function login(req,res){
    if (!isEmpty(req.body)){
        let { email, password } = req.body;
        const user = await User.findOne({email: email});
        if (isEmpty(user)){
            res.status(400).send({
                message: 'USER_NOT_FOUND'
            });
        }else{
            console.log({
                pass: password,
                hash: user.password
            })
            //If the password correspond to the hash
            if (bcrypt.compareSync( password, user.password)){
                let userSalted = await generateNewHash({email, password});
                //console.log(await userSalted);
                let newToken = await jwt.signToken({
                     email: email,
                     expireFrom: Date.now(),
                     expireTo: new Date().addDays(10)
                });
                console.log({
                    newToken: newToken,
                    verified: await jwt.verifyToken(newToken)
                })
                res.setHeader('Authorization', 'Bearer '+ newToken); 
                res.status(201).send({
                    message:{
                        authorization: "Bearer",
                        token: newToken,
                    }
                });
            }else{
                res.status(401).send({
                    message: "UNAUTHORIZED"
                })
            }

        }
    }else{
        res.status(400).send({
            message: 'USER_INFORMATION_NOT_SENT'
        });
    }
}
async function deleteSession(req,res){
    const auth = await User.find({email: email});
    if (isEmpty(auth)){
        throw new Error()
    }
    res.status(400).send('delete')
}

async function create(req, res){
    const { email, username, password } = req.body;

    if (isEmail(email)){
        bcrypt.genSalt(12, function(err, salt) {
            bcrypt.hash(password, salt, async function(err, hash) {
                let newUser = await createUser({ email: email, username: username, password: hash, salt: salt });
                return res.status(201).send(authRessource(newUser));
            });
        });
    }else{
        res.send(400).send({
            message: 'UNVALID_EMAIL'
        })
    } 
}

module.exports = {
    getSession,
    login,
    deleteSession,
    create
}