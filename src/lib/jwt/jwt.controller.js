const jwt = require('jsonwebtoken');
require('dotenv').config();
const SECRET_KEY = process.env.SECRET_KEY;

module.exports.verifyToken = async (string) => {
    try{
        const legit = jwt.verify(string, SECRET_KEY);
        return legit;
    }catch(error){
        throw new Error('TOKEN_NOT_VALID')
    }
}

module.exports.signToken = async (payload) => {
    return jwt.sign(payload, SECRET_KEY);
}

module.exports.decodeToken = async (token) => {
    if (token != null){
       let [ header, payload, signature ] = token.split('.');
       let payloadBuffer = Buffer.from(payload, 'base64');
       return payloadBuffer.toString();
    }
    throw new Error("ERROR_WHEN_DECODING_TOKEN");
}