#!/usr/bin/env node
const { default: isEmail } = require('validator/lib/isEmail');
const yargs = require('yargs');
const { hideBin } = require('yargs/helpers');
const authModel = require("../lib/auth/auth.model");
const authRessource = require('../lib/auth/auth.ressource');
const bcrypt = require("bcrypt");
const db = require('../storage/mongo'); 

yargs(hideBin(process.argv))
  .command(
    'adduser',
    'Add an user in storage',
    (yargs) => {
      return yargs
        .option('email', {
          describe: "Email to add",
          type: "string"
        })
        .option('username', {
          describe: "username to add",
          type: "string"
        })
        .option('password', {
          describe: "Password for the user",
          type: "string"
        })
    },
    (argv) => {
      if (isEmail(argv.email) && argv.password != undefined && argv.password.length > 0 && argv.username != undefined && argv.username.length > 0){
        db.init().then(async ()=>{
          bcrypt.genSalt(12, function(err, salt) {
            bcrypt.hash(argv.password, salt, async function(err, hash) {
                let newUser = await authModel.createUser({
                  username: argv.username,
                  password: hash,
                  email: argv.email
                });
                console.log(authRessource(newUser));
            });
          });
        })
      }
    }
  )
  .help()
  .argv