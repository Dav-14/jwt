const express = require("express");

const router  = express.Router();

router.use('/auth', require('../lib/auth/router'));

module.exports = router;