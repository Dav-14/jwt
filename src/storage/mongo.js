let mongoose = require('mongoose');
require('dotenv').config();

async function init(){
    const URL = `mongodb://${process.env.DB_HOST}:${process.env.DB_PORT}/${process.env.MONGODB_NAME}`;
    return mongoose.connect(URL,{
        user: process.env.ME_CONFIG_MONGODB_ADMINUSERNAME,
        pass: process.env.ME_CONFIG_MONGODB_PASSWORD,
        useNewUrlParser: true,
        useUnifiedTopology: true
    });
}

module.exports = {
    init
};