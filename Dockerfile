FROM node:latest

WORKDIR /app
COPY --chown=node:node . /app
CMD npm install
